#!/bin/sh
# FOLDER=/home/glyconavi/glic/caddy/data
FOLDER=$1
if [ ! -d $FOLDER ]; then
    echo "\n creating folder $FOLDER ..."
    mkdir -p $FOLDER
fi
