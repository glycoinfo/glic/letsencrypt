# sparql.glycoinfo.orgなどのLetsEncrypt SSL サーバー 

[Caddy](https://caddyserver.com/)を使用。

## 基本設定

[CaddyFile](https://gitlab.com/glycoinfo/glic/letsencrypt/-/blob/master/Caddyfile)にはドメイン名の転送を記載するだけで、CaddyのDocker Imageを起動すると、LetsEncryptSSL認証書管理サーバーが設定を読み込みHTTPSを処理する。

## Gitlab CI

一つのサーバーのみとなっており、基本的に当プロジェクトのGit更新が発生すると、サーバーの[Gitlab Runner](https://gitlab.com/glycoinfo/glic/docker-gitlab-runner)が起動し、docker-compose upがサーバー上に起動します。

[.gitlab-ci.yml](https://gitlab.com/glycoinfo/glic/letsencrypt/-/blob/master/.gitlab-ci.yml)
